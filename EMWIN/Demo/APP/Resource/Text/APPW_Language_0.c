/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : APPW_Language_0.c
Purpose     : Conversion into c-array.
              Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "AppWizard.h"

const unsigned char acAPPW_Language_0[14UL + 1] = {
  0x4E, 0x55, 0x4D, 0x3A, 0x31, 0x32, 0x33, 0x34, 0x0A, 0x35, 0x34, 0x33, 0x35, 0x0A, 
};

/*************************** End of file ****************************/
