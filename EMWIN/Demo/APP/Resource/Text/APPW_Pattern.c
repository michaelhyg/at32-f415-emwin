/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : APPW_Pattern.c
Purpose     : Conversion into c-array.
              Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "AppWizard.h"

const unsigned char acAPPW_Pattern[20UL + 1] = {
  0xFF, 0xFE, 0x31, 0x00, 0x32, 0x00, 0x33, 0x00, 0x34, 0x00, 0x35, 0x00, 0x3A, 0x00, 0x4D, 0x00, 0x4E, 0x00, 0x55, 0x00, 
};

/*************************** End of file ****************************/
