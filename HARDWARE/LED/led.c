#include "led.h"

/*AT-START LED resouce array*/
GPIO_Type *LED_GPIO_PORT[LED_NUM] = {LED1_GPIO, LED2_GPIO, LED3_GPIO, LED4_GPIO};
uint16_t LED_GPIO_PIN[LED_NUM]    = {LED1_PIN, LED2_PIN, LED3_PIN, LED4_PIN};
uint32_t LED_GPIO_RCC_CLK[LED_NUM] = {LED1_GPIO_RCC_CLK, LED2_GPIO_RCC_CLK, LED3_GPIO_RCC_CLK, LED4_GPIO_RCC_CLK};

/**
  * @brief  Configure LED GPIO   
  * @param  led: Specifies the LED to be configured.
  * @retval None
  */
void AT32_LEDn_Init(LED_Type led)
{
  GPIO_InitType GPIO_InitStructure;
  /*Enable the LED Clock*/
  RCC_APB2PeriphClockCmd(LED_GPIO_RCC_CLK[led], ENABLE);

  /*Configure the LED pin as ouput push-pull*/
  GPIO_StructInit(&GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pins = LED_GPIO_PIN[led];				 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP; 		 
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;	
  GPIO_Init(LED_GPIO_PORT[led], &GPIO_InitStructure);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4  
  * @retval None
  */
void AT32_LEDn_ON(LED_Type led)
{
  if ( led > (LED_NUM - 1))
    return;
  if ( LED_GPIO_PIN[led] )
    LED_GPIO_PORT[led]->BRE = LED_GPIO_PIN[led];
}

/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4 
  * @retval None
  */
void AT32_LEDn_OFF(LED_Type led)
{
  if ( led > (LED_NUM - 1))
    return;
  if ( LED_GPIO_PIN[led] )
    LED_GPIO_PORT[led]->BSRE = LED_GPIO_PIN[led];
}

/**
  * @brief  Turns selected LED Tooggle.
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4 
  * @retval None
  */
void AT32_LEDn_Toggle(LED_Type led)
{
  if ( led > (LED_NUM - 1))
    return;
  if ( LED_GPIO_PIN[led] )
    LED_GPIO_PORT[led]->OPTDT ^= LED_GPIO_PIN[led];
}

//LED IO��ʼ��
void LED_Init(void)
{
  /*Configure LED in AT_START_BOARD*/
  AT32_LEDn_Init(LED2);
  AT32_LEDn_Init(LED3);
  AT32_LEDn_Init(LED4);
  AT32_LEDn_OFF(LED2);
  AT32_LEDn_OFF(LED3);
  AT32_LEDn_OFF(LED4);
}
 
