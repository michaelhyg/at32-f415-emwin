#ifndef __LED_H
#define __LED_H	 
#include "sys.h"


typedef enum
{
  LED1 = 0,
  LED2,
  LED3,
  LED4
}LED_Type;    

#define LED_NUM     4


#define LED1_PIN                0
#define LED1_GPIO               NULL
#define LED1_GPIO_RCC_CLK       0

#define LED2_PIN                GPIO_Pins_2
#define LED2_GPIO               GPIOC
#define LED2_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOC

#define LED3_PIN                GPIO_Pins_3
#define LED3_GPIO               GPIOC
#define LED3_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOC

#define LED4_PIN                GPIO_Pins_5
#define LED4_GPIO               GPIOC
#define LED4_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOC


void LED_Init(void);//��ʼ��

		 				    
#endif
