/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-08-31 11:22:16
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-31 11:39:59
 */
#ifndef __TIMER_H
#define __TIMER_H
#include "sys.h"


void TIM3_Int_Init(u16 arr,u16 psc);
void TIM2_Int_Init(u16 arr, u16 psc);
#endif
