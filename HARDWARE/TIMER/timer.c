/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-08-31 11:22:16
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-31 11:39:46
 */
#include "timer.h"

extern __IO int32_t OS_TimeMS;

/**
  * @brief  Configure the nested vectored interrupt controller.
  * @param  None
  * @retval None
  */
void NVIC_Configuration(void)
{
    NVIC_InitType NVIC_InitStructure;

    /* Enable the TMR2 global Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TMR2_GLOBAL_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

    NVIC_Init(&NVIC_InitStructure);
}

void TIM2_Int_Init(u16 arr, u16 psc)
{
    TMR_TimerBaseInitType TIM_TimeBaseInitStructure;

    /* TMR2 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1PERIPH_TMR2, ENABLE);

    TIM_TimeBaseInitStructure.TMR_DIV = psc;                       //分频值
    TIM_TimeBaseInitStructure.TMR_CounterMode = TMR_CounterDIR_Up; //计数模式
    TIM_TimeBaseInitStructure.TMR_Period = arr;                    //自动重装数值
    TIM_TimeBaseInitStructure.TMR_ClockDivision = TMR_CKD_DIV1;    //设置时钟分割
    TMR_TimeBaseInit(TMR2, &TIM_TimeBaseInitStructure);

    /* TMR IT enable */
    TMR_INTConfig(TMR2, TMR_INT_Overflow, ENABLE);

    /* TMR2 enable counter */
    TMR_Cmd(TMR2, ENABLE);

    /* NVIC Configuration */
    NVIC_Configuration();
}

void TMR2_GLOBAL_IRQHandler(void) //TIM3中断
{
    if (TMR_GetINTStatus(TMR2, TMR_INT_Overflow) != RESET) //检查指定的TIM中断发生与否:TIM 中断源
    {
        TMR_ClearITPendingBit(TMR2, TMR_INT_Overflow); //清除TIMx的中断待处理位:TIM 中断源
        OS_TimeMS++;
    }
}
