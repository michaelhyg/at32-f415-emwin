#include "tft.h"
#include "FONT.h"
#include "GB1616.h" //16*16汉字字模
#include "delay.h"
#include "spi.h"

#define LCD_TOTAL_BUF_SIZE (240 * 240 * 2)
#define LCD_Buf_Size 1152
static u8 lcd_buf[LCD_Buf_Size];

u16 POINT_COLOR = BLACK; //画笔颜色	默认为黑色
u16 BACK_COLOR = WHITE;  //背景颜色	默认为白色

void LCD_Gpio_Init(void)
{
    GPIO_InitType GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pins = GPIO_Pins_2 | GPIO_Pins_3 | GPIO_Pins_6;
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    // GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    // GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    // GPIO_Init(GPIOB, &GPIO_InitStructure);

    LCD_CS = 0;
    LCD_PWR = 0;

    LCD_RST = 0;
    delay_ms(120);
    LCD_RST = 1;

    SPI1_Init();
}

static void LCD_SPI_Send(u8 *data, u16 size)
{
    for (int i = 0; i < size; i++)
    {
        SPI1_WriteByte(*data++);
    }
}

static void LCD_Write_Cmd(u8 cmd)
{
    LCD_DC = 0;

    LCD_SPI_Send(&cmd, 1);
}

static void LCD_Write_Data(u8 data)
{
    LCD_DC = 1;

    LCD_SPI_Send(&data, 1);
}

void LCD_Write_HalfWord(const u16 da)
{
    u8 data[2] = {0};

    data[0] = da >> 8;
    data[1] = da;

    LCD_DC = 1;
    LCD_SPI_Send(data, 2);
}

void LCD_Address_Set(u16 x1, u16 y1, u16 x2, u16 y2)
{
    if (USE_HORIZONTAL == 0)
    {
        LCD_Write_Cmd(0x2a); //列地址设置
        LCD_Write_Data(x1 >> 8);
        LCD_Write_Data(x1);
        LCD_Write_Data(x2 >> 8);
        LCD_Write_Data(x2);

        LCD_Write_Cmd(0x2b); //行地址设置
        LCD_Write_Data(y1 >> 8);
        LCD_Write_Data(y1);
        LCD_Write_Data(y2 >> 8);
        LCD_Write_Data(y2);

        LCD_Write_Cmd(0x2C); //储存器写
    }
    else if (USE_HORIZONTAL == 1)
    {
        y1 = y1 + 80;
        y2 = y2 + 80;
        LCD_Write_Cmd(0x2a); //列地址设置
        LCD_Write_Data(x1 >> 8);
        LCD_Write_Data(x1);
        LCD_Write_Data(x2 >> 8);
        LCD_Write_Data(x2);

        LCD_Write_Cmd(0x2b); //行地址设置
        LCD_Write_Data(y1 >> 8);
        LCD_Write_Data(y1);
        LCD_Write_Data(y2 >> 8);
        LCD_Write_Data(y2);

        LCD_Write_Cmd(0x2C); //储存器写
    }
    else if (USE_HORIZONTAL == 2)
    {
        LCD_Write_Cmd(0x2a); //列地址设置
        LCD_Write_Data(x1 >> 8);
        LCD_Write_Data(x1);
        LCD_Write_Data(x2 >> 8);
        LCD_Write_Data(x2);

        LCD_Write_Cmd(0x2b); //行地址设置
        LCD_Write_Data(y1 >> 8);
        LCD_Write_Data(y1);
        LCD_Write_Data(y2 >> 8);
        LCD_Write_Data(y2);

        LCD_Write_Cmd(0x2C); //储存器写
    }
    else
    {
        x1 = x1 + 80;
        x2 = x2 + 80;
        LCD_Write_Cmd(0x2a); //列地址设置
        LCD_Write_Data(x1 >> 8);
        LCD_Write_Data(x1);
        LCD_Write_Data(x2 >> 8);
        LCD_Write_Data(x2);

        LCD_Write_Cmd(0x2b); //行地址设置
        LCD_Write_Data(y1 >> 8);
        LCD_Write_Data(y1);
        LCD_Write_Data(y2 >> 8);
        LCD_Write_Data(y2);

        LCD_Write_Cmd(0x2C); //储存器写
    }
}

void LCD_DisplayOn(void)
{
    LCD_PWR = 1;
}

void LCD_DisplayOff(void)
{
    LCD_PWR = 0;
}

void LCD_Clear(u16 color)
{
    u16 i, j;
    u8 data[2] = {0};

    data[0] = color >> 8;
    data[1] = color;

    LCD_Address_Set(0, 0, LCD_Width - 1, LCD_Height - 1);

    for (j = 0; j < LCD_Buf_Size / 2; j++)
    {
        lcd_buf[j * 2] = data[0];
        lcd_buf[j * 2 + 1] = data[1];
    }

    LCD_DC = 1;

    for (i = 0; i < (LCD_TOTAL_BUF_SIZE / LCD_Buf_Size); i++)
    {
        LCD_SPI_Send(lcd_buf, LCD_Buf_Size);
    }
}

void LCD_Fill(u16 x_start, u16 y_start, u16 x_end, u16 y_end, u16 color)
{
    u16 i = 0;
    u32 size = 0, size_remain = 0;

    size = (x_end - x_start + 1) * (y_end - y_start + 1) * 2;

    if (size > LCD_Buf_Size)
    {
        size_remain = size - LCD_Buf_Size;
        size = LCD_Buf_Size;
    }

    LCD_Address_Set(x_start, y_start, x_end, y_end);

    while (1)
    {
        for (i = 0; i < size / 2; i++)
        {
            lcd_buf[2 * i] = color >> 8;
            lcd_buf[2 * i + 1] = color;
        }

        LCD_DC = 1;
        LCD_SPI_Send(lcd_buf, size);

        if (size_remain == 0)
            break;

        if (size_remain > LCD_Buf_Size)
        {
            size_remain = size_remain - LCD_Buf_Size;
        }

        else
        {
            size = size_remain;
            size_remain = 0;
        }
    }
}

void LCD_Draw_Point(u16 x, u16 y)
{
    LCD_Address_Set(x, y, x, y);
    LCD_Write_HalfWord(POINT_COLOR);
}

void LCD_Draw_ColorPoint(u16 x, u16 y, u16 color)
{
    LCD_Address_Set(x, y, x, y);
    LCD_Write_HalfWord(color);
}

void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2)
{
    u16 t;
    int xerr = 0, yerr = 0, delta_x, delta_y, distance;
    int incx, incy, row, col;
    u32 i = 0;

    if (y1 == y2)
    {
        /*快速画水平线*/
        LCD_Address_Set(x1, y1, x2, y2);

        for (i = 0; i < x2 - x1; i++)
        {
            lcd_buf[2 * i] = POINT_COLOR >> 8;
            lcd_buf[2 * i + 1] = POINT_COLOR;
        }

        LCD_DC = 1;
        LCD_SPI_Send(lcd_buf, (x2 - x1) * 2);
        return;
    }

    delta_x = x2 - x1;
    delta_y = y2 - y1;
    row = x1;
    col = y1;

    if (delta_x > 0)
        incx = 1;

    else if (delta_x == 0)
        incx = 0;

    else
    {
        incx = -1;
        delta_x = -delta_x;
    }

    if (delta_y > 0)
        incy = 1;

    else if (delta_y == 0)
        incy = 0;

    else
    {
        incy = -1;
        delta_y = -delta_y;
    }

    if (delta_x > delta_y)
        distance = delta_x;

    else
        distance = delta_y;

    for (t = 0; t <= distance + 1; t++)
    {
        LCD_Draw_Point(row, col);
        xerr += delta_x;
        yerr += delta_y;

        if (xerr > distance)
        {
            xerr -= distance;
            row += incx;
        }

        if (yerr > distance)
        {
            yerr -= distance;
            col += incy;
        }
    }
}

void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2)
{
    LCD_DrawLine(x1, y1, x2, y1);
    LCD_DrawLine(x1, y1, x1, y2);
    LCD_DrawLine(x1, y2, x2, y2);
    LCD_DrawLine(x2, y1, x2, y2);
}

void LCD_Draw_Circle(u16 x0, u16 y0, u8 r)
{
    int a, b;
    int di;
    a = 0;
    b = r;
    di = 3 - (r << 1);

    while (a <= b)
    {
        LCD_Draw_Point(x0 - b, y0 - a);
        LCD_Draw_Point(x0 + b, y0 - a);
        LCD_Draw_Point(x0 - a, y0 + b);
        LCD_Draw_Point(x0 - b, y0 - a);
        LCD_Draw_Point(x0 - a, y0 - b);
        LCD_Draw_Point(x0 + b, y0 + a);
        LCD_Draw_Point(x0 + a, y0 - b);
        LCD_Draw_Point(x0 + a, y0 + b);
        LCD_Draw_Point(x0 - b, y0 + a);
        a++;

        if (di < 0)
            di += 4 * a + 6;
        else
        {
            di += 10 + 4 * (a - b);
            b--;
        }

        LCD_Draw_Point(x0 + a, y0 + b);
    }
}

/**
 * @brief	显示一个ASCII码字符
 *
 * @param   x,y		显示起始坐标
 * @param   chr		需要显示的字符
 * @param   size	字体大小(支持16/24/32号字体)
 *
 * @return  void
 */
void LCD_ShowChar(u16 x, u16 y, char chr, u8 size)
{
    u8 temp, t1, t;
    u8 csize; //得到字体一个字符对应点阵集所占的字节数
    u16 colortemp;
    u8 sta;

    chr = chr - ' '; //得到偏移后的值（ASCII字库是从空格开始取模，所以-' '就是对应字符的字库）

    if ((x > (LCD_Width - size / 2)) || (y > (LCD_Height - size)))
        return;

    LCD_Address_Set(x, y, x + size / 2 - 1, y + size - 1); //(x,y,x+8-1,y+16-1)

    if ((size == 16) || (size == 32)) //16和32号字体
    {
        csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size / 2);

        for (t = 0; t < csize; t++)
        {
            if (size == 16)
                temp = asc2_1608[chr][t]; //调用1608字体
            else if (size == 32)
                temp = asc2_3216[chr][t]; //调用3216字体
            else
                return; //没有的字库

            for (t1 = 0; t1 < 8; t1++)
            {
                if (temp & 0x80)
                    colortemp = POINT_COLOR;
                else
                    colortemp = BACK_COLOR;

                LCD_Write_HalfWord(colortemp);
                temp <<= 1;
            }
        }
    }

    else if (size == 12) //12号字体
    {
        csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size / 2);

        for (t = 0; t < csize; t++)
        {
            temp = asc2_1206[chr][t];

            for (t1 = 0; t1 < 6; t1++)
            {
                if (temp & 0x80)
                    colortemp = POINT_COLOR;
                else
                    colortemp = BACK_COLOR;

                LCD_Write_HalfWord(colortemp);
                temp <<= 1;
            }
        }
    }

    else if (size == 24) //24号字体
    {
        csize = (size * 16) / 8;

        for (t = 0; t < csize; t++)
        {
            temp = asc2_2412[chr][t];

            if (t % 2 == 0)
                sta = 8;
            else
                sta = 4;

            for (t1 = 0; t1 < sta; t1++)
            {
                if (temp & 0x80)
                    colortemp = POINT_COLOR;
                else
                    colortemp = BACK_COLOR;

                LCD_Write_HalfWord(colortemp);
                temp <<= 1;
            }
        }
    }
}

/**
 * @brief	m^n函数
 *
 * @param   m,n		输入参数
 *
 * @return  m^n次方
 */
static u32 LCD_Pow(u8 m, u8 n)
{
    u32 result = 1;

    while (n--) result *= m;

    return result;
}

/**
 * @brief	显示数字,高位为0不显示
 *
 * @param   x,y		起点坐标
 * @param   num		需要显示的数字,数字范围(0~4294967295)
 * @param   len		需要显示的位数
 * @param   size	字体大小
 *
 * @return  void
 */
void LCD_ShowNum(u16 x, u16 y, u32 num, u8 len, u8 size)
{
    u8 t, temp;
    u8 enshow = 0;

    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;

        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                LCD_ShowChar(x + (size / 2) * t, y, ' ', size);
                continue;
            }

            else
                enshow = 1;
        }

        LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size);
    }
}

/**
 * @brief	显示数字,高位为0,可以控制显示为0还是不显示
 *
 * @param   x,y		起点坐标
 * @param   num		需要显示的数字,数字范围(0~999999999)
 * @param   len		需要显示的位数
 * @param   size	字体大小
 * @param   mode	1:高位显示0		0:高位不显示
 *
 * @return  void
 */
void LCD_ShowxNum(u16 x, u16 y, u32 num, u8 len, u8 size, u8 mode)
{
    u8 t, temp;
    u8 enshow = 0;

    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;

        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                if (mode)
                    LCD_ShowChar(x + (size / 2) * t, y, '0', size);
                else
                    LCD_ShowChar(x + (size / 2) * t, y, ' ', size);

                continue;
            }

            else
                enshow = 1;
        }

        LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size);
    }
}

/**
 * @brief	显示字符串
 *
 * @param   x,y		起点坐标
 * @param   width	字符显示区域宽度
 * @param   height	字符显示区域高度
 * @param   size	字体大小
 * @param   p		字符串起始地址
 *
 * @return  void
 */
void LCD_ShowString(u16 x, u16 y, u16 width, u16 height, u8 size, char *p)
{
    u8 x0 = x;
    width += x;
    height += y;

    while ((*p <= '~') && (*p >= ' ')) //判断是不是非法字符!
    {
        if (x >= width)
        {
            x = x0;
            y += size;
        }

        if (y >= height)
            break; //退出

        LCD_ShowChar(x, y, *p, size);
        x += size / 2;
        p++;
    }
}

/**
 * @brief	显示图片
 *
 * @remark	Image2Lcd取模方式：	C语言数据/水平扫描/16位真彩色(RGB565)/高位在前		其他的不要选
 *
 * @param   x,y		起点坐标
 * @param   width	图片宽度
 * @param   height	图片高度
 * @param   p		图片缓存数据起始地址
 *
 * @return  void
 */
void LCD_Show_Image(u16 x, u16 y, u16 width, u16 height, const u8 *p)
{
    if (x + width > LCD_Width || y + height > LCD_Height)
    {
        return;
    }

    LCD_Address_Set(x, y, x + width - 1, y + height - 1);

    LCD_DC = 1;

    LCD_SPI_Send((u8 *)p, width * height * 2);
}

void ST7789_Init(void)
{
    LCD_Gpio_Init(); //硬件接口初始化

    delay_ms(120);
    /* Sleep Out */
    LCD_Write_Cmd(0x11);
    /* wait for power stability */
    delay_ms(120);

    /* Memory Data Access Control */
    LCD_Write_Cmd(0x36);
    if (USE_HORIZONTAL == 0)
        LCD_Write_Data(0x00);
    else if (USE_HORIZONTAL == 1)
        LCD_Write_Data(0xC0);
    else if (USE_HORIZONTAL == 2)
        LCD_Write_Data(0x70);
    else
        LCD_Write_Data(0xA0);

    /* RGB 5-6-5-bit  */
    LCD_Write_Cmd(0x3A);
    LCD_Write_Data(0x65);

    /* Porch Setting */
    LCD_Write_Cmd(0xB2);
    LCD_Write_Data(0x0C);
    LCD_Write_Data(0x0C);
    LCD_Write_Data(0x00);
    LCD_Write_Data(0x33);
    LCD_Write_Data(0x33);

    /*  Gate Control */
    LCD_Write_Cmd(0xB7);
    LCD_Write_Data(0x72);

    /* VCOM Setting */
    LCD_Write_Cmd(0xBB);
    LCD_Write_Data(0x3D); //Vcom=1.625V

    /* LCM Control */
    LCD_Write_Cmd(0xC0);
    LCD_Write_Data(0x2C);

    /* VDV and VRH Command Enable */
    LCD_Write_Cmd(0xC2);
    LCD_Write_Data(0x01);

    /* VRH Set */
    LCD_Write_Cmd(0xC3);
    LCD_Write_Data(0x19);

    /* VDV Set */
    LCD_Write_Cmd(0xC4);
    LCD_Write_Data(0x20);

    /* Frame Rate Control in Normal Mode */
    LCD_Write_Cmd(0xC6);
    LCD_Write_Data(0x0F); //60MHZ

    /* Power Control 1 */
    LCD_Write_Cmd(0xD0);
    LCD_Write_Data(0xA4);
    LCD_Write_Data(0xA1);

    /* Positive Voltage Gamma Control */
    LCD_Write_Cmd(0xE0);
    LCD_Write_Data(0xD0);
    LCD_Write_Data(0x04);
    LCD_Write_Data(0x0D);
    LCD_Write_Data(0x11);
    LCD_Write_Data(0x13);
    LCD_Write_Data(0x2B);
    LCD_Write_Data(0x3F);
    LCD_Write_Data(0x54);
    LCD_Write_Data(0x4C);
    LCD_Write_Data(0x18);
    LCD_Write_Data(0x0D);
    LCD_Write_Data(0x0B);
    LCD_Write_Data(0x1F);
    LCD_Write_Data(0x23);

    /* Negative Voltage Gamma Control */
    LCD_Write_Cmd(0xE1);
    LCD_Write_Data(0xD0);
    LCD_Write_Data(0x04);
    LCD_Write_Data(0x0C);
    LCD_Write_Data(0x11);
    LCD_Write_Data(0x13);
    LCD_Write_Data(0x2C);
    LCD_Write_Data(0x3F);
    LCD_Write_Data(0x44);
    LCD_Write_Data(0x51);
    LCD_Write_Data(0x2F);
    LCD_Write_Data(0x1F);
    LCD_Write_Data(0x1F);
    LCD_Write_Data(0x20);
    LCD_Write_Data(0x23);

    /* Display Inversion On */
    LCD_Write_Cmd(0x21);

    LCD_Write_Cmd(0x29);

    LCD_Address_Set(0, 0, LCD_Width - 1, LCD_Height - 1);

    LCD_Clear(WHITE);

    /*打开显示*/
    LCD_PWR = 1;
}

//宋体16中文
void PutGB1616(u16 x, u16 y, u8 c[2], u8 flag)
{
    unsigned int i, j, k;
    u16 m;

    LCD_Address_Set(x, y, x + 16 - 1, y + 16 - 1);
    for (k = 0; k < 64; k++)
    { //64标示自建汉字库中的个数，循环查询内码
        if ((codeGB_16[k].Index[0] == c[0]) && (codeGB_16[k].Index[1] == c[1]))
        {
            for (i = 0; i < 32; i++)
            {
                m = codeGB_16[k].Msk[i];
                for (j = 0; j < 8; j++)
                {
                    if ((m & 0x80) == 0x80)
                    {
                        LCD_Draw_Point(x + j, y);
                    }
                    else
                    {
                        if (flag)
                            LCD_Draw_Point(x + j, y);
                    }
                    m = m << 1;
                }
                if (i % 2)
                {
                    y++;
                    x = x - 8;
                }
                else
                    x = x + 8;
            }
        }
    }
}

//宋体12中文
void PutGB1212(u16 x, u16 y, u8 c[2], u8 flag)
{
    unsigned int i, j, k;
    u16 m;

    LCD_Address_Set(x, y, x + 12 - 1, y + 12 - 1);
    for (k = 0; k < 64; k++)
    { //64标示自建汉字库中的个数，循环查询内码
        if ((codeGB_12[k].Index[0] == c[0]) && (codeGB_12[k].Index[1] == c[1]))
        {
            for (i = 0; i < 24; i++)
            {
                m = codeGB_12[k].Msk[i];
                for (j = 0; j < 8; j++)
                {
                    if ((m & 0x80) == 0x80)
                    {
                        LCD_Draw_Point(x + j, y);
                    }
                    else
                    {
                        if (flag)
                            LCD_Draw_Point(x + j, y);
                    }
                    m = m << 1;
                }
                if (i % 2)
                {
                    y++;
                    x = x - 8;
                }
                else
                    x = x + 8;
            }
        }
    }
}

//宋体24中文
void PutGB2424(u16 x, u16 y, u8 c[2], u8 flag)
{
    unsigned int i, j, k;
    u16 m;

    LCD_Address_Set(x, y, x + 24 - 1, y + 24 - 1);
    for (k = 0; k < 64; k++)
    { //64标示自建汉字库中的个数，循环查询内码
        if ((codeGB_24[k].Index[0] == c[0]) && (codeGB_24[k].Index[1] == c[1]))
        {
            for (i = 0; i < 72; i++)
            {
                m = codeGB_24[k].Msk[i];
                for (j = 0; j < 8; j++)
                {
                    if ((m & 0x80) == 0x80)
                    {
                        LCD_Draw_Point(x + j, y);
                    }
                    else
                    {
                        if (flag)
                            LCD_Draw_Point(x + j, y);
                    }
                    m = m << 1;
                }

                if ((i % 3 == 0))
                {
                    y++;
                    x = x - 16;
                }
                else
                    x = x + 8;
            }
        }
    }
}

void LCD_PutString(u16 x, u16 y, u8 *s, u8 size, u8 flag)
{
    u8 l = 0;
    while (*s)
    {
        if (size == 16)
        {
            PutGB1616(x + l * 8, y, (u8 *)s, flag);
        }
        else if (size == 12)
        {
            PutGB1212(x + l * 8, y, (u8 *)s, flag);
        }
        else if (size == 24)
        {
            PutGB2424(x + l * 8, y, (u8 *)s, flag);
        }
        s += 2;
        l += 2;
        // 		if( *s < 0x80)
        // 		    {
        // 			LCD_PutChar(x+l*8,y,*s,fColor,bColor,flag);
        // 			s++;l++;
        // 			}
        // 		else
        // 		    {
        // 			PutGB1616(x+l*8,y,(u8*)s,fColor,bColor,flag);
        // 			s+=2;l+=2;
        // 			}
    }
}
