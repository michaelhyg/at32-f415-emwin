/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-08-31 11:05:19
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-31 13:25:57
 */
#include "spi.h"


void SPI1_Init(void)
{
 	GPIO_InitType GPIO_InitStructure;
    SPI_InitType SPI_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOA | RCC_APB2PERIPH_SPI1, ENABLE);

    /* Configure SPI1 pins: SCK, MISO and MOSI ---------------------------------*/
    /* Confugure SCK and MOSI pins as Alternate Function Push Pull */
    GPIO_InitStructure.GPIO_Pins = GPIO_Pins_5 | GPIO_Pins_7;
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    /* Confugure MISO pin as Input Floating  */
    // GPIO_InitStructure.GPIO_Pins = GPIO_Pins_6;
    // GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    // GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* SPI1 configuration ------------------------------------------------------*/
    SPI_DefaultInitParaConfig(&SPI_InitStructure);
    SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;
    SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;
    SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2EDGE;
    SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;
    SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_2;
    SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
    SPI_InitStructure.SPI_CPOLY = 7;
    SPI_Init(SPI1, &SPI_InitStructure);
    /* Enable SPI1 */
    SPI_Enable(SPI1, ENABLE);
}   

void SPI1_WriteByte(u8 TxData)
{
	/* Wait for SPI1 Tx buffer empty */
    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TE) == RESET) {};
    /* Send SPI1 data */
    SPI1->DT = TxData; 
}





















