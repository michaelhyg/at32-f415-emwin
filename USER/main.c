/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-08-28 16:08:41
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-31 13:04:30
 */
#include "GUI.h"
#include "WM.h"
#include "at32f4xx.h"
#include "delay.h"
#include "led.h"
#include "sys.h"
#include "tft.h"
#include "timer.h"

void APPW_X_FS_Init(void) {

}

int main(void)
{
    delay_init();
    LED_Init();
    //SysclkConfig(PLL_HSE, ClockTable[8].Sysclk);
    ST7789_Init();
    RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_CRC, ENABLE); //使能CRC时钟，否则STemWin不能使用
    WM_SetCreateFlags(WM_CF_MEMDEV);
    
    TIM2_Int_Init(999, 143); //1KHZ 定时器1ms

    MainTask();
    //GUIDEMO_Main();
    for (;;)
    {
    }
}

#ifdef USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {}
}
#endif
